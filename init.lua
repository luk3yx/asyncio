--
-- Minetest asyncio-style thing
--

asyncio = {}

local modpath = minetest.get_modpath('asyncio')
dofile(modpath .. '/core.lua')
dofile(modpath .. '/transpile.lua')

asyncio.domodfile('locks.lua')
-- asyncio.domodfile('test.lua')
